#!/bin/bash

# exit when any command fails; be verbose
set -ex

# make cmsrel etc. work
shopt -s expand_aliases
export MY_BUILD_DIR=${PWD}
#export MY_REL=CMSSW_4_4_7
#export MY_GT=GR_R_44_V15
export MY_REL=CMSSW_3_9_2_patch5
#export MY_REL=CMSSW_3_11_3
export MY_GT=GR_R_39X_V6B
echo pwd
pwd
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/cmsusr
cmsrel ${MY_REL}
mkdir -p ${MY_REL}/src/AnalysisCode/TriggerInfo
mv ${MY_BUILD_DIR}/* ${MY_REL}/src/AnalysisCode/TriggerInfo
cd ${MY_REL}/src
cmsenv
cd AnalysisCode/TriggerInfo
scram b
ln -sf /cvmfs/cms-opendata-conddb.cern.ch/${MY_GT} ${MY_GT}
ln -sf /cvmfs/cms-opendata-conddb.cern.ch/${MY_GT}.db ${MY_GT}.db
ls
cmsRun python/triggerinfoanalyzer_cfg_${MY_GT}.py > trigger.out
sudo cp trigger.out /builds/kati/hi-test
ls -l /builds/kati/hi-test


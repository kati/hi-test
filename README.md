# Test reading 2010 and 2011 PbPb data

Testing reading of GTs GR_R_44_V15 and GR_R_39X_V6B from /cvmfs for a file in the HIHighPt (2011) and HICorePhysics (2010) datasets. 
The changes to [the original](https://github.com/cms-opendata-analyses/trigger_examples/tree/2010/TriggerInfo/TriggerInfoAnalyzer) 
for 2011 dataset (in python/triggerinfoanalyzer_cfg_GR_R_39X_V6B.py):


*  runs on `CMSSW_4_4_7`
*  GT `GR_R_44_V15`
*  input file `root://eospublic.cern.ch//eos/opendata/cms/hidata/HIRun2011/HIHighPt/RECO/15Apr2013-v1/110000/10EF1D2D-77B4-E211-A652-003048F1C494.root`
*  input to the EDAnalyzer `datasetName = cms.string("HIHighPt")`
 
For 2010 dataset (in python/triggerinfoanalyzer_cfg_GR_R_39X_V6B.py)

*  runs on `CMSSW_3_9_2_patch5`
*  GT `GR_R_39X_V6B`
*  input file `root://eospublic.cern.ch//eos/opendata/cms/hidata/HIRun2010/HICorePhysics/RECO/PromptReco-v3/000/150/881/B41883EA-FFED-DF11-8A61-001617C3B6C6.root`
*  input to the EDAnalyzer `datasetName = cms.string("HICorePhysics")`

The CI test pipeline runs the script in `.gitlab/readhi.sh` where one can choose the CMSSW version and GT. 
The output log is passed as an artifact and can be downloaded after a successful job.

Find the detailed description of the trigger info analyzer functionality in its
[own repository](https://github.com/cms-opendata-analyses/trigger_examples/tree/2010/TriggerInfo/TriggerInfoAnalyzer).


